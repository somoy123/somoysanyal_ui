import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router, RouterLink } from "@angular/router";
import * as $ from 'jquery'

@Component({
  selector: 'app-renter-dashboard',
  templateUrl: './renter-dashboard.component.html',
  styleUrls: ['./renter-dashboard.component.css']
})
export class RenterDashboardComponent implements OnInit {
  msg;
  constructor(private route:ActivatedRoute,private router : Router) { }

  ngOnInit() {
    $(document).ready(()=>{
      $('#slide-toggle').click(()=>{
        $('#toggle-content').slideToggle('slow');
    });
    });

    this.msg=sessionStorage.getItem('uname');
    console.log(this.msg);
  }
  logout(){
    sessionStorage.removeItem('uname');
    this.router.navigate(['/'])
    window.location.reload();
  }
}
