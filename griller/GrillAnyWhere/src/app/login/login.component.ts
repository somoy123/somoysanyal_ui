import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from "@angular/router";
import { ReactiveFormsModule, FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LoginService } from '../login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private userForm:FormGroup
  private user:any
  errorMessage:string
  private isSaved:boolean
  private users:any[]

  constructor(private builder:FormBuilder,private router : Router,private service:LoginService) { 
  this.buildForm()
  }
  ngOnInit() {
  }
  buildForm() {
    this.userForm = this.builder.group({
     
      email: ['',[
        Validators.required,
        Validators.email
      ]
      ],
      password: ['',Validators.required],
    })
  }
  save() {
    this.user={
    email:this.userForm.controls['email'].value,
    password:this.userForm.controls['password'].value
    }
    this.service.loginVerify(this.user.email,this.user.password,(result)=>{
    console.log(result)
    console.log(result[0].email)
    if(result[0].email==this.user.email&&result[0].password==this.user.password){
      sessionStorage.setItem('ownername',result[0].name);
      this.router.navigate(['./owner-dashboard']);
      window.location.reload();
    }else
    {
      alert("please provide valid data to login");
      this.userForm.reset();
      
    }
    })
  }
  saveData() {
    this.user={
    email:this.userForm.controls['email'].value,
    password:this.userForm.controls['password'].value
    }
    this.service.loginVerifyUser(this.user.email,this.user.password,(result)=>{
    console.log(result)
    console.log(result[0].email)
    if(result[0].email==this.user.email&&result[0].password==this.user.password){
      console.log(result[0].name);
      sessionStorage.setItem('uname',result[0].name);
      this.router.navigate(['./renter-dashboard']);
      window.location.reload();
    }else
    {
      alert("please provide valid data to login")
      this.userForm.reset();
    }
    })
  }
    
    
  }


