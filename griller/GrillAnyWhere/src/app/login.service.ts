import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private _url:string = 'http://localhost:5200'

  constructor(private http:HttpClient) { }
  loginVerify(name,email,callback){
    this.http
      .get(this._url+'/owner/find/'+name+'/'+email)
      .subscribe(data=>{
        callback(data)
      },error=>{
        console.log('Unable to Process the request')
      })
     }

     loginVerifyUser(name,email,callback){
      this.http
        .get(this._url+'/user/find/'+name+'/'+email)
        .subscribe(data=>{
          callback(data)
        },error=>{
          console.log('Unable to Process the request')
        })
       }
  }

